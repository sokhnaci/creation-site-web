<?php
session_start();

 ?>
 <!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>PAGE CLASSEMENT</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>
  <body background= "photos/background.jpg" style="background-repeat: repeat;background-size: 100%;background-attachment:fixed;">
    <img src="photos/bandeau.jpg" alt="..." height="275" width="100%">
    <nav class="navbar navbar-expand-md navbar-light bg-info">
  <a class="navbar-brand" href="#">BIENVENUE <?php echo $_SESSION['email']; ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="pageprincipaluser.php">VOTE<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="connexion.php">DECONNEXION</a>
    </div>
  </div>
</nav>
<?php
              include 'infoconnexionbdd.php';
              $db=mysqli_connect("localhost","root","","conceptionsiteweb");
               $requete=("SELECT * FROM projetping ORDER BY nbrevote DESC ");
               $exec_requete = mysqli_query($db,$requete);

              // $reponse      = mysqli_fetch_array($exec_requete);
               if(!$exec_requete){
               die('Echec de la requête SQL :'.$mysqli->error);
             }else{
                   ?>

                   <center>
                     <div container_fluid>
                      <h1 class=" text-light font-weight-bold">CLASSEMENT </h1>
               	<table border="1" width="40%">
                  <style>
                  table thead tr th { background:#FFFF66;/jaune/ } /* thead : 1ere ligne */
                  table thead tr th:first-child { background:#FFFF66; /gris/ } /* thead : 1ere ligne, 1ere colonne */
                  table tbody tr td { background:#fff; } /* tbody : cellules courantes */
                  </style>
               	<thead>
               		<tr>

               			<th>Nom Projet</th>
               			<th>Votes</th>
               		</tr>
               	</thead>
               	<tbody>
               <?php
               	// pour chaque ligne (chaque enregistrement)
                while($tuple=mysqli_fetch_array($exec_requete))
               	{
               		// DONNEES A AFFICHER dans chaque cellule de la ligne
               ?>
               		<tr>

               			<td><?php echo $tuple['nom_projet']; ?></td>
               			<td><?php echo $tuple['nbrevote']; ?></td>
               		</tr>
               <?php
               	} // fin foreach
               ?>
               	</tbody>
               	</table>
              </div>
              </center>
              <?php

             }
              ?>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
