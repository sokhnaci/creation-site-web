<?php
session_start();

 ?>
 <!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>PAGE MODIFIER</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

  </head>
  <body background= "photos/background.jpg" style="background-repeat: repeat;background-size: 100%;background-attachment:fixed;">
    <img src="photos/bandeau.jpg" alt="..." height="275" width="100%">
    <nav class="navbar navbar-expand-md navbar-light bg-info">
  <a class="navbar-brand" href="#">BIENVENUE DANS NOTRE PLATEFORME</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="index.php">ACCUEIL<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="ajouter.php">AJOUTER</a>
      <a class="nav-item nav-link" href="modifier.php">MODIFIER</a>
      <a class="nav-item nav-link" href="classement.php">CLASSEMENT</a>
      <a class="nav-item nav-link" href="connexion.php">DECONNEXION</a>
      <form class="form-inline my-2 my-lg-0" action="traitementmodifier.php" method="post">
      <input class="form-control mr-sm-2" type="search" placeholder="Recherchez le projet a modifier" aria-label="Search" name="nomP">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Recherche</button>
    </form>
    </div>
  </div>
</nav>

<form >
<div class="form-group col-md-6">
<label for="inputPrenom">AUTEUR</label>
<input type="text" class="form-control" id="inputPrenom" placeholder="Auteur" name="auteur">
</div>
<div class="form-group col-md-6">
<label for="inputPrenom">DESCRIPTION</label>
<input type="text" class="form-control" id="inputPrenom" placeholder="Decription du projet" name="description">
</div>
</div>
<form>
<div class="form-group col-md-6">
<label for="exampleFormControlFile1">TELECHARGER UN FICHIER</label>
<input type="file" class="form-control-file" id="exampleFormControlFile1" name="poster">
</div>
</form>
<button type="submit" class="btn btn-success">MODIFIER</button>
</div>
</form>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
