<?php
session_start();

 ?>
 <!DOCTYPE html>
<html lang="fr" >
  <head>
    <meta charset="utf-8">
    <title>AJOUTER UN POSTER</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>
  <body background= "photos/background.jpg" style="background-repeat: repeat;background-size: 100%;background-attachment:fixed;">
    <img src="photos/bandeau.jpg" alt="..." height="275" width="100%">
    <nav class="navbar navbar-expand-md navbar-light bg-info">
    <a class="navbar-brand" href="#">BIENVENUE <?php echo $_SESSION['email']; ?></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="pageprincipal.php">VOTE<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="classement2.php">CLASSEMENT</a>
      <a class="nav-item nav-link" href="gestiondesElections.php">GESTION DES ELECTIONS</a>
      <a class="nav-item nav-link" href="index.php">DECONNEXION</a>
    </div>
    </div>
    </nav>
    <div class="container">
      <form action="traitementajouter.php" method="post" enctype="multipart/form-data">


      <div class="form-row">
    <div class="form-group col-md-6">
      <label for="inputNom">NOM</label>
      <input type="text" class="form-control" id="inputNom" placeholder="Nom du projet" name="nomP">
    </div>
    <div class="form-group col-md-6">
      <label for="inputPrenom">AUTEUR</label>
      <input type="text" class="form-control" id="inputPrenom" placeholder="Auteur" name="auteur">
    </div>
    </div>
    <div class="form-row">
      <div class="form-group col-md-6">
        <label for="exampleFormControlTextarea1">DESCRIPTION</label>
        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description"></textarea>
      </div>
  <div class="container-fluid">

    <div class="form-group col-md-6">
      <label for="exampleFormControlFile1">TELECHARGER UN FICHIER</label>
      <input type="file" class="form-control-file" id="exampleFormControlFile1" name="poster">

    </div>
      <button type="submit" class="btn btn-success">AJOUTER</button>
</div>
</div>

</form>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
