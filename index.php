<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ACCUEIL</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css"


  </head>
  <body background= "photos/background.jpg" style="background-repeat: repeat;background-size: 100%;background-attachment:fixed;">
    <img src="photos/bandeau.jpg" alt="..." height="275" width="100%">
    <nav class="navbar navbar-expand-md navbar-light bg-info">
  <a class="navbar-brand" href="#">BIENVENUE DANS NOTRE PLATEFORME</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link active" href="index.php">ACCUEIL<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="connexion.php">CONNEXION</a>
      <a class="nav-item nav-link" href="inscription.php">INSCRIPTION</a>
    </div>
  </div>
</nav>
<div class="container-fluid">
<div class="row">
  <div class="col-md-6 text-light">
    <h1>QU'EST CE QU'UN PROJET PING?</h1>
<p>Dans le cadre des Projets Ingénieurs, les étudiants ont présenté leur poster illustrant leur projet.
  Le Projet Ingénieur consiste à conduire un projet intégrant l’ensemble des dimensions technologiques,
   organisationnelles, financières et humaines. Il est mené en équipe de 6 étudiants et s’étend sur la 2e
   et 3e année du cycle ingénieur.Les projets sont proposés principalement par des entreprises.
   Ils peuvent être liés à des projets de recherche ou être à l’initiative des étudiants s’il s’agit
   d’une création d’entreprise. Chaque équipe a pour mission de :
   rechercher le sujet auprès d’entreprises ou de structures de recherche et de développement
   réaliser une étude de faisabilité technico économique validée par la structure donneuse d’ordre
   élaborer le cahier des charges
   réaliser le projet
   exposer publiquement le résultat de ses travaux
   Chaque groupe est suivi par un binôme d’encadrement et un instructeur. Il peut avoir recours à
   des experts pointus sur des points précis du projet.</p>
  </div>


<article class="col-md-6 ">
<div class="pull-right">
  <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active">
          <img src="photos/microp.jpg" class="d-block w-50" alt="..." width="">
      </div>
      <div class="carousel-item">
        <img src="photos/bg.jpg" class="d-block w-50" alt="...">
      </div>
      <div class="carousel-item">
        <img src="photos/vote.jpg" class="d-block w-50" alt="...">
      </div>
    </div>
  </div>
</div>
</article>

</div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
