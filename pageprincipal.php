<?php
session_start();
 ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>PAGE D'ACCUEIL</title>
    <!-- css -->
    <link rel="stylesheet" href="css/bootstrap.min.css">

  </head>
  <body background= "photos/background.jpg" style="background-repeat: repeat;background-size: 100%;background-attachment:fixed;">
    <img src="photos/bandeau.jpg" alt="..." height="275" width="100%">
    <nav class="navbar navbar-expand-md navbar-light bg-info">
  <a class="navbar-brand" href="#">BIENVENUE <?php echo $_SESSION['email']; ?></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="ajouter.php">AJOUTER<span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="classement2.php">CLASSEMENT</a>
      <a class="nav-item nav-link" href="gestiondesElections.php">GESTION DES ELECTIONS</a>
      <a class="nav-item nav-link" href="connexion.php">DECONNEXION</a>
    </div>
  </div>
</nav>

<form action="traitementvoteAdmin.php" method="post">
  <?php include 'traitementaffichage.php' ?>
<div class="form-group col-md-6">
  <label for="exampleInputNom">Nom Projet</label>
  <input type="text" class="form-control" id="exampleInputNom" aria-describedby="nomHelp" placeholder="Enter le nom du projet que vous voulez voter" name="nomP">
  <input type="submit" value="VOTER" class="btn btn-success">
</div>

</form>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
